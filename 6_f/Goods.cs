﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6_f
{
    public class Goods
    {
        
        public string GoodsName { get; set; }

        public double  GoodsPrice { get; set; }

        public override string ToString()
        {
            return GoodsName + " 单价:" + GoodsPrice;
        }
        public Goods() { }
        public Goods(string name,double price)
        {
            GoodsName = name;
            GoodsPrice = price;
        }
       
    }
}
