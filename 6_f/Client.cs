﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6_f
{
    public  class Client
    { 
        public string ClientName { get; set; }

       
         public Client(string name)
          {
                 ClientName = name;
          }
        public Client()
        {
            ClientName = null;
        }

        public override string ToString()
        {
              return "客户："+ClientName;
        }

    }
}
