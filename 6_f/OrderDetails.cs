﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6_f
{
    public class OrderDetails
    {
        public int DetailID { get; set; }
        public Goods? goods { get; set; }
        public int Number { get; set; }
        public double DetailPrice { get; set; }

        public OrderDetails (int detailID, Goods? goods, int number)
            {
               this.DetailID = detailID;
               this.Number = number;
                this.goods = goods;
            this.DetailPrice = number * goods.GoodsPrice;
              
            }
        public OrderDetails()
        {
            this.DetailID = 0;
            this.Number = 0;
            this.goods = new Goods();
            this.DetailPrice = this.Number * goods.GoodsPrice;

        }

        public override string ToString()
        {

            return "[detail:" + DetailID + "] " + goods+ "  数量：" + Number+ " 价格：" + DetailPrice + " ";
        }
        public override bool Equals(object obj)
        {
            var obj1 = obj as OrderDetails;
            return obj1 != null && this.DetailID ==obj1.DetailID;
        }
        public override int GetHashCode()
        {
            return HashCode.Combine(DetailID);
        }
    }
}
