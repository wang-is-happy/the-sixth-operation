﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _6_f
{
    public partial class Form5 : Form
    {
        public OrderService orderservice = new OrderService();
        public Form5(OrderService  orderService)
        {
            InitializeComponent();
            this.orderservice = orderService;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (radioButton1.Checked)
                {
                    int index = 0;
                    int id = Convert.ToInt32(textBox1.Text);

                    bindingSource1.DataSource = orderservice.Orders.Where(s => s.OrderId == id);
                    var select1 =
                        from a in orderservice.Orders
                        where a.OrderId == id
                        orderby a.allprice
                        select a;
                    foreach (var x in select1)
                     {
                    bindingSource2.DataSource = select1;
                        index++;
                   }
                    if (index == 0)
                        MessageBox.Show("未查询到目标！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else if (radioButton2.Checked)
                {
                    int index = 0;
                    string name = textBox1.Text;
                    List<Order> selectit = new List<Order>();
                    foreach (var x in orderservice.Orders)
                    {
                        List<OrderDetails> sss = new List<OrderDetails>();
                        sss = x.Orderdetails.FindAll(b => b.goods.GoodsName == name);
                        if (sss.Count > 0)
                        {
                            selectit.Add(x);
                        }
                    }
                    var select2 =
                        from a in selectit
                        where a.Orderdetails.Count > 0
                        orderby a.allprice
                        select a;


                    foreach (var x in select2)
                    {

                        bindingSource1.DataSource = x;
                        bindingSource2.DataSource = x.Orderdetails;
                        index++;
                    }
                    if (index == 0)
                        MessageBox.Show("未查询到目标！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else if (radioButton3.Checked)
                {

                    int index = 0;
                    string name2 = textBox1.Text;
                    bindingSource1.DataSource = orderservice.Orders.Where(s => s.client.ClientName==name2);
                    var select3 =
                        from a in orderservice.Orders
                        where a.client.ClientName == name2
                        orderby a.allprice
                        select a;
                    foreach (var x in select3)
                    {
                       
                        bindingSource2.DataSource = x.Orderdetails;
                        index++;
                    }
                    if (index == 0)
                        MessageBox.Show("未查询到目标！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
                else if (radioButton4.Checked)
                {

                    int index = 0;
                    int money = Convert.ToInt32(textBox1.Text);
                    bindingSource1.DataSource = orderservice.Orders.Where(s => s.allprice == money);
                    var select4 =
                        from a in orderservice.Orders
                        where a.allprice == money
                        orderby a.allprice
                        select a;
                    foreach (var x in select4)
                    {
                        
                        bindingSource2.DataSource = x.Orderdetails;
                        index++;
                    }
                    if (index == 0)
                    {
                        MessageBox.Show("未查询到目标！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else if (radioButton5.Checked)
                {
                    bindingSource1.DataSource = orderservice.Orders;
                    foreach (var x in orderservice.Orders)
                    {
                        
                        bindingSource2.DataSource = x.Orderdetails;
                    }

                }
            }catch (Exception ex) { MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }
    }
}
