﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _6_f
{
    public partial class Form3 : Form
    {
        public OrderService orderService1 = new OrderService();
        public Form3(OrderService  orderService )
        {
            InitializeComponent();
            this.orderService1 = orderService;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

            int modifynum = Convert.ToInt32(textBox1.Text);
            int exist = 0;
            foreach (Order order in orderService1.Orders)
            {
                if (order.OrderId == modifynum)
                {
                    DialogResult success = System.Windows.Forms.MessageBox.Show("成功找到订单！",
                                      "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    exist++;
                    order.client = new Client(textBox2.Text);
                    DialogResult success4 = System.Windows.Forms.MessageBox.Show("客户名修改成功！",
                                      "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    try
                    {
                        int exist1 = 0;
                        foreach (OrderDetails orderdetail in order.Orderdetails)
                        {
                            if (orderdetail.DetailID == Convert.ToInt32(textBox3.Text))
                            {
                                DialogResult success1 = MessageBox.Show("成功找到订单明细！",
                                     "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                exist1++;
                                orderdetail.goods.GoodsName = textBox7.Text;
                                       
                             
                                 order.allprice -= orderdetail.DetailPrice;
                                 orderdetail.goods.GoodsPrice = Convert.ToDouble(textBox5.Text);
                                 orderdetail.DetailPrice = orderdetail.goods.GoodsPrice * orderdetail.Number;
                                 order.allprice += orderdetail.DetailPrice;

                                 order.allprice -= orderdetail.DetailPrice;
                                        orderdetail.Number = Convert.ToInt32(textBox4.Text );
                                        orderdetail.DetailPrice = orderdetail.goods.GoodsPrice * orderdetail.Number;
                                order.allprice += orderdetail.DetailPrice;
                             

                            }
                            DialogResult success2 = System.Windows.Forms.MessageBox.Show("修改成功！",
                                    "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);



                        }
                        if (exist1 == 0)
                        {

                            DialogResult error = System.Windows.Forms.MessageBox.Show("未找到订单明细！",
                                                  "提示", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    catch (Exception ex) { MessageBox.Show("操作失败！" + ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Error); }
                }
            }
            if(exist == 0)
            {
               System.Windows.Forms.MessageBox.Show("未找到订单！请重新输入",
                                      "提示", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
