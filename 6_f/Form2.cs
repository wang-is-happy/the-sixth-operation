﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace _6_f
{
    public partial class Form2 : Form
    {
        OrderService _orderService;
        Order _order;
        public Form2(OrderService orderService)
        {
            InitializeComponent();
            _orderService = orderService;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
           
            Client client = new(textBox1.Text);
         
            int OrderID = (Convert.ToInt32(textBox2.Text));
             _order = new(OrderID, client);
            if (_orderService.IsInOrders(_order))
            {
                DialogResult Stop = System.Windows.Forms.MessageBox.Show("此订单已经存在，请重新输入",
                                "提示", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                _orderService.Orders.Add(_order);
                DialogResult success = System.Windows.Forms.MessageBox.Show("添加成功！",
                                      "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form2_1 form2_1= new Form2_1(_orderService,_order);
            form2_1.StartPosition = FormStartPosition.CenterParent;
            form2_1.ShowDialog();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }
    }
}
