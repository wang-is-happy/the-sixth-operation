﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace  _6_f

{

    public class OrderService
    {
        public List<Order>? Orders { get; set; }

        public OrderService ()
        {
            Orders = new List<Order>();
        }
         public  bool IsInOrders(Order order)
        {

            if (Orders == null)
                return false;
            for (int i = 0; i < Orders.Count; i++)
            {
                if (order.Equals(Orders.ElementAt(i)))
                {
                    return true;
                }
            }
            return false;
        }
      

        public void AddOrder()
        {
            try
            {


                Console.WriteLine("请输入客户的名称：");
                Client client = new(Console.ReadLine());
                Console.WriteLine("请输入账单的ID：");
                int OrderID = (Convert.ToInt32(Console.ReadLine()));
                Order order = new(OrderID, client);
                while (IsInOrders(order))
                {
                    Console.WriteLine("此订单已存在！请重新输入。");
                    System.Console.WriteLine("请输入客户的名称：");
                    client = new(Console.ReadLine());
                    System.Console.WriteLine("请输入账单的ID：");
                    OrderID = Convert.ToInt32(Console.ReadLine());
                    order = new(OrderID, client);
                }
                Orders.Add(order);
                Console.WriteLine("添加账单成功！");


                Console.WriteLine("是否添加账单明细？请输入true或false");
                bool isaddorderdetail = Convert.ToBoolean(Console.ReadLine());
                while (isaddorderdetail)
                {
                    order.addOrderDetails();
                    Console.WriteLine("是否添加账单明细？请输入数字true或false");
                    isaddorderdetail = Convert.ToBoolean(Console.ReadLine());
                }
                Console.WriteLine("操作完成！");
            }catch (Exception ex) { Console.WriteLine("操作失败！"+ex.Message); }
        }

        public void DeleteOrder()
        {
            try
            {
                Console.WriteLine("请输入需要删除的账单ID");
                int deletenum = Convert.ToInt32(Console.ReadLine());
                int exist = 0;
                foreach (Order order in Orders)
                {
                    if (order.OrderId == deletenum)
                    {
                        Console.WriteLine("找到该订单");
                        Orders.Remove(order);
                        Console.WriteLine("操作成功！");
                        exist++;
                        break;
                    }
                }
                if (exist == 0) Console.WriteLine("操作失败！未找到该订单");
            }
            catch (Exception ex)
            { Console.WriteLine("操作失败！ " + ex); }
                    
           // Console.WriteLine("操作成功！");
        }
        public void ModifyOrder()
        {
            try
            {
                Console.WriteLine("请输入需要修改的账单ID");
                int modifynum = Convert.ToInt32(Console.ReadLine());
                int exist = 0;
                foreach (Order order in Orders)
                {
                    if (order.OrderId == modifynum)
                    {
                        Console.WriteLine("找到该账单，若选择修改客户名输入0，或者输入账单细则ID");
                        int detailID = Convert.ToInt32(Console.ReadLine());
                        if(detailID == 0)
                        {
                            Console.WriteLine("请输入新客户名称：");
                            order.client =new Client(Console.ReadLine());
                            Console.WriteLine("修改成功！");
                        }
                        else
                        {
                            order.ModifyOrderDetails(detailID);
                        }
                        exist++;

                        break;
                    }
                }
                if (exist == 0) Console.WriteLine("操作失败！未找到该订单");
            }
            catch (Exception ex) { Console.WriteLine("操作失败！"+ex); }
        }
        internal void SortOrder()
        {
            try
            {if (Orders.Count  == 0)
                {
                    Console.WriteLine("订单列表为空！");
                    
                }
                else
                {
                    Orders.Sort(new MyComparer());
                    Console.WriteLine("对订单进行排序成功！");
                }
            }catch (Exception ex) { Console.WriteLine("排序操作失败！"+ex); }
           
        }
        public void QueryOrder()
        {
            if(Orders.Count == 0)
            {
                Console.WriteLine("订单列表为空！");
                return;
            }
            Console.WriteLine("请输入序号选择查询依据：" + "\n" + "1.订单号" + "\n" + "2.商品名称" + "\n" + "3.客户" + "\n" + "4.订单金额"+"\n"+"5.查询全部订单");
            int choice=Convert.ToInt32(Console.ReadLine());
            int index = 0;
            switch (choice)
            {
                
                case 1:
                    
                    Console.WriteLine("请输入订单ID：");
                    int id=Convert.ToInt32(Console.ReadLine());
                    var select1 =
                        from a in Orders
                        where a.OrderId == id
                        orderby a.allprice 
                        select a;
                    foreach (var x in select1)
                    {
                        Console.WriteLine(x);
                        index++;
                    }
                    break;
                case 2:
                    Console.WriteLine("请输入商品名称：");
                    string name = Console.ReadLine();
                    List<Order> selectit = new List<Order>();
                    foreach (var x in Orders)
                    {
                        List<OrderDetails > sss = new List<OrderDetails >();
                        sss=x.Orderdetails.FindAll(b => b.goods.GoodsName == name);
                        if(sss.Count > 0)
                        {
                            selectit.Add(x);
                        }
                    }
                    var select2 =
                        from a in selectit
                        where a.Orderdetails.Count > 0
                        orderby a.allprice
                        select a;
                    foreach (var x in select2)
                    { 
                        Console.WriteLine(x);
                        index++;
                    }
                    break;
                case 3:
                    Console.WriteLine("请输入客户名称：");
                    string name2 = Console.ReadLine();
                    var select3 =
                        from a in Orders
                        where a.client.ClientName  == name2
                        orderby a.allprice
                        select a;
                    foreach (var x in select3)
                    {
                        Console.WriteLine(x);
                        index++;
                    }
                    break;
                case 4:
                    Console.WriteLine("请输入订单金额：");
                    int money = Convert.ToInt32(Console.ReadLine());
                       
                    var select4 =
                        from a in Orders
                        where a.allprice == money
                        orderby a.allprice
                        select a;
                    foreach (var x in select4)
                    {
                        Console.WriteLine(x);
                        index++;
                    }
                    break;
                case 5:
                    foreach(var x in Orders)
                        Console.WriteLine(x);
                    index++;
                    break;
                default:
                    Console.WriteLine("输入错误！");
                    break;


            }
            if (index == 0) Console.WriteLine("未查询到目标！");


            
        }
        public   void Export()
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(List<Order>));
            using (FileStream fs = new FileStream("data.xml", FileMode.Create)) {
                xmlSerializer.Serialize(fs, Orders);
              }
        }
        public  void Import()
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(List<Order>));
            try
            {
                using (FileStream fs = new FileStream("data.xml", FileMode.Open))
                {
                    Orders = (List<Order>?)xmlSerializer.Deserialize(fs);
                }
            }
            catch (Exception ex)
            { throw ex; }
        }
    }
}
