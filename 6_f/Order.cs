﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6_f
{
    [Serializable]
    public  class Order : IComparable
    {
        public int OrderId { get; set; }
        public Client? client { get; set; }
   

        public List<OrderDetails> Orderdetails { get; set; }

        public double  allprice { get; set; }

        public Order(int id, Client client)
        {
            OrderId = id;
            this.client = client; 
            Orderdetails = new List<OrderDetails>();
            
        }

        public Order ()
        {
            OrderId=0;
            client = new Client();
            Orderdetails = new List<OrderDetails>();
        }
        public override bool Equals(object? obj)
        {
            Order od = obj as Order;
            return od != null && OrderId == od.OrderId;
        }

        public override string ToString()
        {

            string s = "订单ID:" + OrderId + " " + client + " 订单总价： " + allprice + "\n";
            foreach (OrderDetails  item in Orderdetails) s += item + "\n";
            return s;
        }


        public bool IsInOrderDetails(OrderDetails orderDetail)
        {
            if(Orderdetails == null) return false;

            for (int i = 0; i < Orderdetails.Count; i++)
            {
                if (orderDetail.Equals (Orderdetails.ElementAt (i)))
                {
                    return true;
                }
            }

            return false;
        }
        public void addOrderDetails ()
        {
            Goods goods = new ();
            System.Console.WriteLine("请输入商品的名称：");
            goods.GoodsName= Console.ReadLine();
            System.Console.WriteLine("请输入"+goods.GoodsName +"的单价：");
            goods.GoodsPrice =Convert.ToDouble ( Console.ReadLine());
            System.Console.WriteLine("请输入" + goods.GoodsName + "的数量：");
            int num = (Convert.ToInt32(Console.ReadLine()));

            System.Console.WriteLine("请输入账单明细的ID：");
            int detailID = (Convert.ToInt32(Console.ReadLine()));
            OrderDetails orderdetail = new(detailID, goods, num);
            while (IsInOrderDetails(orderdetail))
            {
                Console.WriteLine("此订单明细已存在！请重新输入。");
                System.Console.WriteLine("请输入商品的名称：");
                goods.GoodsName = Console.ReadLine();
                System.Console.WriteLine("请输入" + goods.GoodsName + "的单价：");
                goods.GoodsPrice = Convert.ToDouble(Console.ReadLine());
                System.Console.WriteLine("请输入" + goods.GoodsName + "的数量：");
                 num = (Convert.ToInt32(Console.ReadLine()));

                System.Console.WriteLine("请输入账单明细的ID：");
                detailID = (Convert.ToInt32(Console.ReadLine()));
                orderdetail = new(detailID, goods, num);
            }
            Orderdetails.Add (orderdetail );
            Console.WriteLine("添加账单明细成功！");
            allprice += orderdetail.DetailPrice;
        }
        public void ModifyOrderDetails(int orderdetailID)
        {
            try
            {
                int exist = 0;
                foreach (OrderDetails orderdetail in Orderdetails)
                {
                    if (orderdetail.DetailID == orderdetailID)
                    {
                        Console.WriteLine("找到该订单明细");

                        Console.WriteLine("请输入需要修改的内容，输入1修改商品名称，输入2修改商品价格，输入3修改商品数目");
                        int choice = Convert.ToInt32(Console.ReadLine());
                        switch (choice)
                        {
                            case 1:
                                Console.WriteLine("请输入商品名称：");
                                orderdetail.goods.GoodsName = Console.ReadLine();
                                exist++;
                                break;
                            case 2:
                                Console.WriteLine("请输入商品价格：");
                                allprice -= orderdetail.DetailPrice;
                                orderdetail.goods.GoodsPrice = Convert.ToDouble(Console.ReadLine());
                                orderdetail.DetailPrice = orderdetail.goods.GoodsPrice * orderdetail.Number;
                                allprice += orderdetail.DetailPrice;
                                exist++;
                                break;
                            case 3:
                                Console.WriteLine("请输入商品数目：");
                                allprice -= orderdetail.DetailPrice;
                                orderdetail.Number = Convert.ToInt32(Console.ReadLine());
                                orderdetail.DetailPrice = orderdetail.goods.GoodsPrice * orderdetail.Number;
                                allprice += orderdetail.DetailPrice;
                                exist++;
                                break;
                            default:
                                Console.WriteLine("修改失败！输入不正确！");
                                break;


                        }
                        
                        Console.WriteLine("修改成功！");
                        break;
                    }
                }
                if (exist == 0) Console.WriteLine("操作失败！未找到该订单明细");
            }
            catch (Exception ex) { Console.WriteLine("操作失败！" + ex.Message); }
        }
        public int CompareTo(object obj2)
        {
            if (!(obj2 is Order ))
                throw new System.ArgumentException();
            Order  p2 = (Order)obj2;
            return this.OrderId  - p2.OrderId;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(OrderId);
        }
    }

    public   class MyComparer:IComparer<Order>
    {
        public int Compare(Order order1,Order order2)
        {
            return order1.OrderId - order2.OrderId;
        }
    }
}
