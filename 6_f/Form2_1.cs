﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _6_f
{
    public partial class Form2_1 : Form
    {
        public OrderService _service;
        public Order _order=new Order();
        public Form2_1(OrderService orderService,Order order)
        {
            InitializeComponent();
            _service = orderService;
            _order = order;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Goods goods = new();
            goods.GoodsName = textBox1.Text;
           
            goods.GoodsPrice = Convert.ToDouble(textBox3.Text);
           
            int num = (Convert.ToInt32(textBox4.Text));

           
            int detailID = (Convert.ToInt32(textBox2.Text));
            OrderDetails orderdetail = new(detailID, goods, num);
            if (_order.IsInOrderDetails(orderdetail))
            {
                DialogResult Stop = System.Windows.Forms.MessageBox.Show("此订单明细已经存在，请重新输入",
                                "提示", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                _order.Orderdetails.Add(orderdetail);
                DialogResult success = System.Windows.Forms.MessageBox.Show("添加成功！",
                                      "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                _order.allprice += orderdetail.DetailPrice;
            }
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
