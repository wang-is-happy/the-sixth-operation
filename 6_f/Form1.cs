

namespace _6_f
{
    public partial class Form1 : Form
    {
        OrderService _orderService=new OrderService();
        public Form1()
        {
            InitializeComponent();
            _orderService.Import();
        }

        private void Form1_MouseEnter(object sender, EventArgs e)
        {
            
        }

        

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            Form2 form2 = new Form2(_orderService);
            form2.StartPosition = FormStartPosition.CenterParent;
            form2.ShowDialog();

        }

        private void label1_Click(object sender, EventArgs e)
        {
           
        }

        private void flowLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            Form3 form3 = new Form3(_orderService);
            form3.StartPosition = FormStartPosition.CenterParent;
            form3.ShowDialog();

        }

        private void button6_Click(object sender, EventArgs e)
        {
            _orderService.Export();
            MessageBox.Show("您已经成功退出系统！");
            this.Close();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (_orderService.Orders.Count == 0)
            {
                MessageBox.Show("订单列表为空！",
                                 "提示", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            else
            {
                _orderService.Orders.Sort(new MyComparer());
                System.Windows.Forms.MessageBox.Show("对订单排序成功！",
                                       "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form4 form4 = new Form4(_orderService);
            form4.StartPosition = FormStartPosition.CenterParent;
            form4.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (_orderService.Orders.Count == 0)
            {
                MessageBox.Show("订单列表为空",
                                      "提示", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                Form5 form5 = new Form5(_orderService);
                form5.StartPosition = FormStartPosition.CenterParent;
                form5.ShowDialog();
            }
        }
    }
}