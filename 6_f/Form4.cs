﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _6_f
{
    public partial class Form4 : Form
    {
        public OrderService orderservice=null;
        public Form4(OrderService  orderService)
        {
            InitializeComponent();
            this.orderservice = orderService;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                int deletenum = Convert.ToInt32(textBox1.Text);
                int exist = 0;
                foreach (Order order in orderservice.Orders)
                {
                    if (order.OrderId == deletenum)
                    {

                        orderservice.Orders.Remove(order);
                        System.Windows.Forms.MessageBox.Show("删除成功！",
                                           "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        exist++;
                        break;
                    }
                }
                if (exist == 0)
                {
                    MessageBox.Show("未找到该订单！",
                                              "提示", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
